# DVS Tests

## tdaq-09-00-00

Added `generate_core_file.sh` script which can be used as a TestFailure follow-up action to automatically generate a core file for a misbehaving application process (to allow debuging rare conditions with applications). 

```
Usage: generate_core_file.sh [-v] [-h] [-d <directory_name>] -p <partition_name> -a <app_name> [-m mail_address]"
```

Parameters for particular application are deduced from the configuration in runtime, see DVS/TestManager documentation for more details on configuring this in the test repository: https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltTestManager#How_to_Parametrize_Tests
