/*
test_app_log:	Tests if a log file of an application started by PMG is created.
		Log file name should be read from database.

A.Kazarov, 2003
*/

#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

int main(int argc, const char * const * argv)
{
   CmdArgStr	filename	('f', "file", "file-name", "log file name to test", CmdArg::isREQ );
   CmdArgBool	verbose         ('v', "verbose", "turn on 'verbose'-mode.");

   CmdLine  cmd(*argv, &filename, &verbose, NULL);
   cmd.description("Tests if a log file of an application started by PMG is created. "
		   "Log file name should be read from database.") ;
   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);

   TestResult result = TmPass;

// check if the file exists	
   std::ostringstream txt ;
   if ( access(filename, R_OK) == -1 )
	switch ( errno )
	{
		case EACCES:
 			txt<<"File "<<(const char* )filename<<" does not have execute or read permissions." ;
			ers::error(ers::Message( ERS_HERE, txt.str()));	
			result = TmFail ;
			break ;
		case ELOOP:
 			txt<<"Too many symbolic links in resolving "<< (const char* )filename;
			ers::error(ers::Message( ERS_HERE, txt.str()));	
			result = TmFail ;
			break ;
		case ENOTDIR:
		case EFAULT:
		case ENOENT:
 			txt<< "File "<<(const char* )filename<<" does not exist.";
			ers::error(ers::Message( ERS_HERE, txt.str()));	
			result = TmFail ;
			break ;
		default:
 			txt<<"Got '"<<strerror(errno)<<"' error trying to access file "<<(const char*)filename ;
			ers::error(ers::Message( ERS_HERE, txt.str()));	
			result = TmUnresolved ;
	}

   return result ;
}
