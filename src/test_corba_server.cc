/*	Tests if a CORBA-based application is running in the partition by 
	calling 'lookup' and making 'ping' of the object.
	Should be run on ahy host in a TCP/IP network in IPC environment.

	S.Kolos, 1999
	
    	A.Kazarov, Dec 2003: new IPC/ORB
	no context-name anymore, only server name
*/

#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/object.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

int main(int argc, char ** argv)
{	
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

  CmdArgStr     partition_name	('p', "partition", "partition-name", "partition to work in, global as default");
  CmdArgStr	context_name	('c', "context", "context-name", "name of the context server belongs to", CmdArg::isREQ);
  CmdArgStr	server_name	('n', "name", "object-name", "name of the object to test", CmdArg::isREQ);
  CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");

  CmdLine  cmd(*argv, &partition_name, &context_name, &server_name, &verbose, NULL);
  cmd.description("Tests if a CORBA-based application is running in the given partition  "
		  "by calling 'lookup' and making 'ping' to the object."
		  "Can be run on ahy host on a network in IPC environment.") ;

  CmdArgvIter  arg_iter(--argc, ++argv);

       // Parse arguments
  cmd.parse(arg_iter);
  
  IPCPartition   p ( partition_name ) ;

  if ( !p.isValid() )
  {
	std::ostringstream txt ;
 	txt<<"Cannot access partition server for partition "<< (const char*)partition_name ;
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	return TmFail;  		
  }

  int result = TmPass ;
  
  try 	{ 
	ipc::servant_var object 	= p.lookup<ipc::servant, ipc::no_cache, ipc::non_existent >(std::string(server_name), std::string(context_name) );
	ipc::servant::ApplicationContext_var info 	= object->app_context() ;
	}
  catch ( const daq::ipc::InvalidPartition & ex )
	{
 	ers::warning(ex);	
	result = TmFail;  	
  	}
  catch ( const daq::ipc::InvalidObjectName & ex )
	{
	ers::warning(ex);
	result = TmFail;  	
  	}
  catch ( const daq::ipc::ObjectNotFound & ex )
	{
	std::ostringstream txt ;
 	txt<<"Object "<<(const char*)server_name<<" was not found in partition "<<(const char*)partition_name;
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	result = TmFail;  	
  	}
  catch ( CORBA::SystemException & ex )
	{
	ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &ex)) ;	
	result = TmFail ;
	}

  return result;
}
