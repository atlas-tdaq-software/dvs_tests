/*
Tests IS server in a given partition by publishing and querying objects on it.

    S.Kolos, 1999

    A.Kazarov, Dec 2003: new IPC/ORB

*/

#include <signal.h>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/alarm.h>
#include <is/infoany.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

static CmdArgBool   verbose ('v', "verbose", "turn on 'verbose'-mode.");
static CmdArgStr	server_name('n', "server", "server-name", "server to test.", CmdArg::isREQ);
static short		errCount;
static short		callbackCount;
static short		queryCallbackCount;
static const double	timeout_period = 3.0;
static std::string	TestInfoName("ISTestInfo_");

static void query_callback(ISCallbackInfo * isc){
	queryCallbackCount++;
	if ( verbose )
		{
		ERS_LOG("QUERY CALLBACK:: "<< isc->name()<<" "<<isc->time()<<": "<< isc->reason() );
		}
	
	if( callbackCount == 3 && queryCallbackCount == 3 )
		kill(getpid(), SIGINT);
}

static void callback(ISCallbackInfo * isc){
	callbackCount++;
	if ( verbose )
		ERS_LOG("CALLBACK:: "<< isc->name() << " "<<isc->time()<< ": " << isc->reason() );
	
	if( callbackCount == 3 && queryCallbackCount == 3 )
		kill(getpid(), SIGINT);
}

static bool timeout_callback(void * param){
	//ISInfoReceiver * ir = (ISInfoReceiver*)param;
	if ( verbose )
		ERS_LOG( "Timeout" );
	kill(getpid(), SIGINT);
        return false ;
}

static bool TestInfo(void * param)
{
  IPCPartition * 	p = (IPCPartition*)param;
  ISInfoDictionary	id(*p);
  
  std::string		name(server_name);
  name += ".";
  name += TestInfoName;	
  
  ISInfoLong	isl = 11132L;
  
  try	{
  	id.insert(name.c_str(),isl) ;
	}
  catch ( daq::is::Exception & ex )
  	{	
	std::ostringstream txt ;
 	txt<<"Insert failed: " <<ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return false ; 
  	}

  if ( verbose )
	ERS_LOG( "Insert successfull" );
  
  try 	{
	id.update(name.c_str(),isl)  ;
	}
  catch ( daq::is::Exception & ex )
  	{	
	std::ostringstream txt ;
 	txt<<"Update failed: " << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return false ; 
  	}

  if ( verbose )
	ERS_LOG( "Update successfull" );
  
  ISInfoAny       isa;
  try 	{
	id.findValue(name.c_str(),isa)  ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Find value failed: " << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return false ; 
  	}

  if ( verbose )
	ERS_LOG( "Find value successfull" );
  
  try	{
	id.remove(name.c_str())  ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Remove value failed: " << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return false ; 
  	}

  if ( verbose )
	ERS_LOG( "Remove successfull " );          

  return false ; // do not call it again
}

static void DoTest(IPCPartition & p)
{
  ISInfoReceiver	ir(p);
  
  std::string		name(server_name);
  name += ".";
  name += TestInfoName;	

  try	{
	ir.subscribe(name.c_str(), callback, &ir)  ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Subscribe to " << name << " fails: " << ex ;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return ; 
  	}

  if ( verbose )
	ERS_LOG( "Subscribe to " << name << " successfull" );
		  
  try	{
	ir.subscribe(std::string(server_name), ISInfoLong::type()&&TestInfoName, query_callback, &ir)  ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Query 'Subscribe to all' fails with error: " << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return ; 
  	}

  if ( verbose )
   	ERS_LOG( "Query 'Subscribe to all' successfull" ); 
  
  IPCAlarm * to_alarm = new IPCAlarm(timeout_period, timeout_callback, &ir) ;
  IPCAlarm * do_alarm = new IPCAlarm(0, TestInfo, &p) ;

  signal(SIGINT, [](int) {; }) ;
  signal(SIGQUIT, [](int) {; }) ;
  signal(SIGTERM, [](int) {; }) ;

  ::sleep(-1) ;

  delete to_alarm ;
  delete do_alarm ;

  try	{
	ir.unsubscribe(name.c_str())  ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Unsubscribe to " << name << " fails: " << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return ; 
  	}

  if ( verbose )
	ERS_LOG( "Unsubscribe to " << name << " successfull" ) ;

  
  try	{
	ir.unsubscribe((const char*)server_name, ISInfoLong::type()&&TestInfoName) ;
	}
  catch ( daq::is::Exception & ex )
  	{
	std::ostringstream txt ;
 	txt<<"Query 'Unsubscribe to all' fails:" << ex;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	errCount++ ;
	return ; 
  	}
  if ( verbose )
   	ERS_LOG( "Query 'Unsubscribe to all' successfull" ); 		
  
  if ( queryCallbackCount != 3 )
  {
	std::ostringstream txt ;
 	txt<<"Wrong number of query callback invocations : "<< queryCallbackCount;
	ers::error(ers::Message( ERS_HERE, txt.str()));	 	
  }
  else
  {
  	if ( verbose )
		ERS_LOG( "Query callback invoked "<<queryCallbackCount<<" times." );
  }

  if ( callbackCount != 3 )
  {
	std::ostringstream txt ;
 	txt<<"Wrong number of callback invocations : "<< callbackCount;
	ers::error(ers::Message( ERS_HERE, txt.str()));	 	
  }
  else
  {
  	if ( verbose )
		ERS_LOG( "Callback invoked " << callbackCount << " times." );
  }
}

int main(int argc, char ** argv)
{
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

      // Declare arguments
      
  CmdArgStr	partition_name('p', "partition", "partition-name", "Partition to work in.");
  verbose = false;	 
       // Declare command object and its argument-iterator
  CmdLine  cmd(*argv, &verbose, &partition_name, &server_name, NULL) ;
  cmd.description("Tests IS server in a given partition by publishing and querying objects on it. ") ;
  CmdArgvIter  arg_iter(--argc, ++argv) ;
  
       // Parse arguments
  cmd.parse(arg_iter);
  
  IPCPartition		p(partition_name);
 
  if ( !p.isValid() )
	{
	std::ostringstream txt ;
 	txt<<"Cannot access partition server for partition "<< (const char*)partition_name ;
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	return TmFail;  		
	}
 
  TestResult ret = TmPass;
  
  char pid[32];
  char hostname[256];
  hostname[0] = 0;
  
  gethostname(hostname, sizeof(hostname));
  sprintf(pid,"%lu",(unsigned long)getpid());
  
  TestInfoName += hostname;
  TestInfoName += "-";
  TestInfoName += pid; 
  DoTest(p);

  if ( errCount || callbackCount != 3 || queryCallbackCount != 3 )
  	ret = TmFail;

  return ret;
}
