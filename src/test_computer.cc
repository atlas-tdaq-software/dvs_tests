/*	Tests computer by execution 'hostname -f' and 'getcmtconfig -h' commands via remote shell
	and comparing returned values with ones stored in conf. database
	Uses 'RemoteLogin' attribute of a Computer object in database.
	Can be run on ahy host in a TCP/IP network.
	Test failed on timeout of 20 seconds.
	
	A.Kazarov, 2000

	Dec 2003: new parameters, -t - HW_Tag attribute from database, replaced "system"
		  TmUnresolved is returned instead of TmUntested

Jan 2003: introduced internal timeout
*/

#include <sys/poll.h>
#include <cstdlib>
#include <cerrno>
#include <iostream>
#include <cstdio>
#include <string>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <tmgr/tmresult.h>

#define DEF_TIMEOUT	20

// these default options make sence for SSH method only
#define DEF_RLOGIN_OPTS	"-nfx"

using namespace daq::tmgr ;
    
int main(int argc, const char * argv[] )
{
   CmdArgStr 	hostname	('H', "Host",		"host-name", "host name or IP address",CmdArg::isREQ);
   CmdArgStr 	hwtag		('t', "tag",		"hardware-tag", "HW_Tag from database {i686-slc3, x86_64_slc4}",CmdArg::isREQ);
   CmdArgStr 	login_cmd	('l', "logincmd",	"login-command", "Remote login command (e.g. rsh or ssh)",CmdArg::isREQ);
   CmdArgBool	verbose		('v', "verbose",	"turn on 'verbose' mode.");
   CmdArgInt 	connect_timeout	('T', "timeout",	"connect-timeout", "Timeout to connect to remote computer, 20 sec default.");
   verbose = false;
   try
	{ IPCCore::init( argc, (char**)argv ) ; }
   catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
   catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore
 
   CmdLine  cmd(*argv, &verbose, &hostname, &login_cmd, &hwtag, &connect_timeout, NULL);

   cmd.description( "Tests computer by execution of 'hostname' and 'getcmtconfig' command via remote shell "
   		    "and comparing returned value with HW_Tag attribute of a computer from the configuration.\n"
		    "Verifies that the computer is able to run pmg_agent via remote shell.\n"
		    "The way of remote command ivocation is defined in 'RLogin' attribute.\n"
		    "The test should be run on ahy host in a TCP/IP network.\n"
		    "Failed on timeout (30 seconds by default)." ) ;

   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);
   
   std::string osname(hwtag) ;

   const char * rlogin_opts = getenv("TDAQ_RLOGIN_OPTS") ;
   if ( !rlogin_opts )
  	{
 	ERS_DEBUG(1,"TDAQ_RLOGIN_OPTS environment not defined, using default value " << DEF_RLOGIN_OPTS );
	rlogin_opts = DEF_RLOGIN_OPTS ;
  	}

   char command[512] ;
   sprintf(	command, "%s %s %s \"hostname -f\" 2>/dev/null", (const char *)login_cmd,
		rlogin_opts, (const char *)hostname ) ;

   if ( verbose )
	ERS_LOG("Executing command: "<< command );

   int timeout = connect_timeout.flags() && CmdArg::GIVEN ? connect_timeout :  DEF_TIMEOUT ;
   //timeval	t_alarm = { timeout, 0 } ;

   FILE* stream = popen(command, "r") ;
   if ( !stream ) // test was not executed
   	{
   	std::ostringstream txt ;
 	txt<<"Can not execute command '"<<command<<"'. Remote test was not started. Error description: "<< strerror(errno);
 	ers::warning (ers::Message( ERS_HERE, txt.str()));	
	return TmUnresolved ;
	}
  
  struct pollfd ufds = { fileno(stream), POLLIN, 0 } ;
  if ( !poll( &ufds, 1, timeout*1000) )
	{
   	std::ostringstream txt ;
 	txt<<"Timeout ("<<timeout<<" seconds) reached trying to connect to target computer.\n\tComputer can not be accessed.";
 	ers::warning (ers::Message( ERS_HERE, txt.str()));	
   	return TmFail ;
	}

   char buf[1024] ;
   std::string output;
   
   // seeking for the last line in the output
   while ( fgets(buf, sizeof(buf)-1, stream) )  
  	{
	buf[strlen(buf)-1] = 0 ;
   	output = buf;
   	}
   
   if ( output.empty() ) // empty file => rsh was unsuccessful
   	{
	ers::warning (ers::Message( ERS_HERE,"Remote shell was unable to execute command 'hostname'." ));	
	pclose(stream) ;	
    	return TmFail ;
   	}
      
   if ( output != std::string(hostname) )
   	{
   	std::ostringstream txt ;
 	txt<<"Hostname '"<<buf<<"' returned by "<<(const char*)hostname<<" is not correct." ;
	ers::warning (ers::Message( ERS_HERE, txt.str()));	
	pclose(stream) ;	
   	return TmFail ;
   	}
 
   pclose(stream) ;	
 
 // then test HW_tag

   const char * prefix = getenv("TDAQC_INST_PATH") ;

   if ( !prefix )
  	{
	ers::warning (ers::Message( ERS_HERE, "Can not get TDAQC_INST_PATH environment. Check the configuration of this test." ));	
   	return TmUnresolved ;
   	}

   sprintf(command, "%s %s %s \"%s/share/bin/getcmtconfig -hw\"",(const char *)login_cmd, rlogin_opts, (const char *)hostname, prefix);

   if ( verbose )
	ERS_LOG("Executing command: "<< command ) ;
   
   stream = popen(command, "r") ;
   if ( !stream ) // test was not executed
   	{
   	std::ostringstream txt ;
 	txt<<"Can not execute command '"<<command<<"'. Remote test was not started. Error description: "<< strerror(errno) ;
 	ers::warning (ers::Message( ERS_HERE, txt.str()));		
   	return TmUnresolved ;
	}

   // seeking for the last line in the output
   while ( fgets(buf, sizeof(buf)-1, stream) )  
	{
	buf[strlen(buf)-1] = 0 ;
   	output = buf;
	}

   if ( output.empty() ) // empty file => rsh was unsuccessful
	{
 	ers::warning (ers::Message( ERS_HERE, "Remote shell was unable to execute command 'hostname'." ));			
	pclose(stream) ;	
    	return TmFail ;
	}
   
   if ( output != osname )
   {   
      std::ostringstream txt ;
      txt<<"HW_Tag '"<<buf<<"' returned by "<<(const char*)hostname<<" does not correspond to the database value '"<<osname<<"'." ;
      ers::warning (ers::Message( ERS_HERE, txt.str()));	
      pclose(stream) ;	
      return TmFail ;
   }
   else
   {
      pclose(stream) ;
      return TmPass ;
   }
}

