/*
 test_app_pmg:	Tests if the information about running process is in PMG.
 Used to verify that applications (like MRS receiver etc) that have
 no real test, are really running.

 A.Kazarov, 2004
 */
#include <iostream>
#include <set>
#include <string>
#include <functional>
#include <future>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ProcessManager/client_simple.h>
#include <system/Host.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr;

int main(int argc, char ** argv) {
    try {
        IPCCore::init(argc, argv);
        daq::pmg::Singleton::init();
    }
    catch(daq::ipc::CannotInitialize & ex) {
        ers::warning(ex);
        return TmUnresolved;
    }
    catch(daq::ipc::AlreadyInitialized & ex) {
        ;
    } // just ignore
    catch(daq::pmg::Exception & ex) // shall not happen
    {
        ers::warning(ex);
        return TmUnresolved;
    };

    CmdArgStr partition_name('p', "partition", "partition-name", "partition name", CmdArg::isREQ);
    CmdArgStrList hosts('H', "host", "host-names...", "the hosts where application could be running (more than one in case of back-up hosts)", CmdArg::isREQ);
    CmdArgStr app_name('a', "application", "application-name", "Application name (as in ConfDB)", CmdArg::isREQ);
    CmdArgBool verbose('v', "verbose", "turn on 'verbose'-mode");
    verbose = false;
    CmdLine cmd(*argv, &partition_name, &hosts, &app_name, &verbose, NULL);
    cmd.description("Tests if the PMG recognizes the application as running.");
    CmdArgvIter arg_iter(--argc, ++argv);
    cmd.parse(arg_iter);

    std::set<std::string> appHosts;
    for(unsigned int i = 0; i < hosts.count(); ++i) {
        appHosts.insert(std::string(hosts[i]));
    }

    const std::string partName(partition_name);
    const std::string appName(app_name);

    std::function<TestResult (const std::string&)> lookup = [&partName, &appName, &verbose] (const std::string& hostName) -> TestResult {
        System::Host h(hostName);

        try {
            auto handle_ptr = daq::pmg::Singleton::instance()->lookup(h.full_name(),
                                                                                   appName,
                                                                                   partName);
            if( !handle_ptr ) {
                if(verbose)
                    ERS_LOG("Application " << appName << " is not found on host " << h.full_name());

                return TmFail;
            }

            auto proc = daq::pmg::Singleton::instance()->get_process(*handle_ptr);
            if( !proc ) {
                if(verbose)
                    ERS_LOG("Application " << appName << " is not running on host " << h.full_name());

                return TmFail;
            }

            if(proc->status().state == pmgpub::RUNNING) {
                if(verbose)
                    ERS_LOG("Application " << appName << " is found in running state on host " << h.full_name());

                return TmPass;
            }
	    else
		{
                if(verbose)
                    ERS_LOG("Application " << appName << " is found in state " << proc->status().state << " on host " << h.full_name());
		
		}
        }
        catch(daq::pmg::Exception & ex) {
            if(verbose) {
                ERS_LOG("Application " << appName << " could not be looked-up on host " << h.full_name());
                ers::warning(ex);
            }
        }

        return TmUnresolved;
    };

    std::vector<std::future<TestResult>> jobResults;
    for(const std::string& c : appHosts) {
        jobResults.push_back(std::async(appHosts.size() == 1 ? std::launch::deferred : std::launch::async, lookup, c));
    }

    std::set<TestResult> lookupResults;
    for(auto& fut : jobResults) {
        lookupResults.insert(fut.get());
    }

    if(lookupResults.find(TmPass) != lookupResults.end()) {
        return TmPass;
    }

    if(lookupResults.find(TmUnresolved) != lookupResults.end()) {
        return TmUnresolved;
    }

    return TmFail;
}
