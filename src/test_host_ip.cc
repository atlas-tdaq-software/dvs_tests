// Tests host's name and/or IP address by establishin a TCP/IP connection to it
// Detects EHOSTDOWN, EHOSTUNREACH, ETIMEDOUT and ECONNREFUSED TCP/IP errors
//
// Author: A.Kazarov, 1999
//

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

static struct in_addr * atoaddr(const char* address)
{
   struct hostent *		host;
   static struct in_addr	saddr;

  /* First try it as aaa.bbb.ccc.ddd. */
  
   saddr.s_addr = inet_addr(address);
   if ( saddr.s_addr != (unsigned long)-1 )
	return &saddr;
   host = gethostbyname(address);
   if (host != 0)
	return (struct in_addr *) *host->h_addr_list;

   return 0;
}

int main(int argc, const char * const * argv)
{
   CmdArgStr 	hostname	('H',"Host", "host-name", "host name or IP address",CmdArg::isREQ);
   CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");

   CmdLine  cmd(*argv, &hostname, &verbose, NULL);
   cmd.description("Tests host's name and/or IP address by establishing a TCP/IP connection to it.\n"
		   "Detects EHOSTDOWN, EHOSTUNREACH, ETIMEDOUT and ECONNREFUSED TCP/IP errors.") ;

   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);
   
   TestResult result = TmPass;

// here some testing goes
   
   int port = 1; // nevermind, we just check for address is OK
   struct in_addr *	addr;
   struct sockaddr_in 	address;

   addr = atoaddr(hostname);
   if (addr == 0)
   {
	std::ostringstream txt ;
 	txt<<"Address "<<(const char*)hostname<<" has invalid format, can not resolve.";
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	result = TmFail ;
   }
   else
   {
	memset((char *) &address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = port;
	address.sin_addr.s_addr = addr->s_addr;

	int sock = socket(AF_INET,SOCK_STREAM,0);
	int connected = connect(sock, (struct sockaddr *) &address, sizeof(address));

	if ( connected == -1 )
	{
	        std::ostringstream txt ;
		switch ( errno )
		{	
			case EHOSTDOWN:
			case EHOSTUNREACH:
			case ETIMEDOUT: 
			// no route to host
 	        		txt<<"Host IP Test failed (errno "<<errno<<")";
	 			ers::warning(ers::Message( ERS_HERE, txt.str()));	
				result = TmFail ;
				break ;
			case 0:
			case ECONNREFUSED:
				break ;
			default:
				txt<<"Host IP Test unresolved (errno "<<errno<<")";
				ers::warning(ers::Message( ERS_HERE, txt.str()));
				result = TmUnresolved ;
				break ;
        	}
	}
        else 
		close(sock);
   }
   return result ;
}
