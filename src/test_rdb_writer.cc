/*	Tests functionality of the named RDB server in the given partition.
	Retrieves number of classes from the server. Test failed if this number is 0.
	Should be run on ahy host in IPC environment.

	A.Kazarov,	2000
	Dec 2003: new ORB/IPC
*/

#include <iostream>
#include <string>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <tmgr/tmresult.h>
#include <rdb/rdb.hh>
#include <rdb/rdb_info.h>

using namespace daq::tmgr ;

struct WriterSession {
  rdb::writer_var cursor;
  rdb::RDBSessionId session;
  RDBWriterPingCB * ping_cb;
  rdb::ProcessInfo proc_info;

  WriterSession() : session(0), ping_cb(0) {;}

  ~WriterSession();
};

WriterSession::~WriterSession()
{
  if(session) {
    try {
      cursor->close_session( session, proc_info );
    }
    catch(CORBA::SystemException & ex) {
      ers::error(daq::ipc::CorbaSystemException( ERS_HERE, &ex )) ;
      exit(TmFail);
    }
    catch(rdb::SessionNotFound& ex) {
      std::ostringstream txt ;
      txt<< "RDBServer::close_session() failed with rdb::SessionNotFound exception: session " << ex.session_id << " is not found";
      ers::error(ers::Message( ERS_HERE, txt.str() ));
      exit(TmFail);
    }
  }
}


int main(int argc, char ** argv)
{
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

  CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work in");
  CmdArgStr	server_name	('n', "name", "server-name", "RDB server name");
  CmdArgBool	verbose 	('v', "verbose", "turn on 'verbose'-mode.");
  CmdArgStr     db_var_name	('a', "db-variable-name", "variable-name", "read database server name form process environment with given name instead of -n option");
  
  CmdLine  cmd(*argv, &verbose, &partition_name, &server_name, &db_var_name, NULL) ;
  cmd.description("Tests functionality of named RDB writer server in the given partition.\n"
		  "Checks if server is available and has non-zero number of classes loaded."
		  "Can be run on ahy host in IPC environment.") ;
  CmdArgvIter  arg_iter(--argc, ++argv) ;
  
  server_name = "" ;

  cmd.parse(arg_iter) ;

  if( !(server_name.flags() & CmdArg::GIVEN) ) {
    if( !(db_var_name.flags() & CmdArg::GIVEN) ) {
 		  ers::error (ers::Message( ERS_HERE,"RDB server name is not provided (use -n or -a option)" ));		
      return TmUnresolved ;
    }
    else {
      const char * v = getenv( static_cast<const char *>(db_var_name) );
      if ( v && *v ) {
        server_name = v;
      } else	{
        std::ostringstream txt ;
        txt<<"Server name is not found since the environment variable " << static_cast<const char *>(db_var_name) << " mentioned by command line is not set or is empty.";
        ers::error (ers::Message( ERS_HERE, txt.str()));		        		
        return TmUnresolved ;
      }
    }
  }

  IPCPartition p(partition_name) ;
  if ( !p.isValid() ) {
   	std::ostringstream txt ;
 		txt<<"Cannot access partition server for partition "<< (const char*)partition_name ;
 		ers::error (ers::Message( ERS_HERE, txt.str()));		
	  return TmFail ;  		
	}


    // the 'writer' contains several objects used for communication with read-write server
    // the destructor closes session

  WriterSession writer;

	// find server object in partition and open session

  try 	{
    writer.cursor = p.lookup<rdb::writer>(std::string(server_name));
    rdb::ProcessInfoHelper::set(writer.proc_info);
    writer.ping_cb = new RDBWriterPingCB();
    writer.session = writer.cursor->open_session( writer.proc_info, writer.ping_cb->_this() );
    writer.ping_cb->set_session_id(writer.session);
	}
  catch ( CORBA::SystemException & ex ) {
    ers::error(daq::ipc::CorbaSystemException( ERS_HERE, &ex ));
    return TmFail ;
  }
  catch ( daq::ipc::Exception & ex ) {
    ers::error( ex ) ;
    return TmFail ;
	}
         
  rdb::RDBClassList_var l ;
  try {
	  writer.cursor->get_all_classes(writer.session, l);
  }
  catch ( const rdb::SessionNotFound& ex ) {
    std::ostringstream txt ;
    txt<< "RDBServer::get_all_classes() failed with rdb::SessionNotFound exception: session " << ex.session_id << " is not found";
    ers::error(ers::Message( ERS_HERE, txt.str() ));
    exit(TmFail);
  }
  catch ( CORBA::SystemException & ex ) {
    ers::error(daq::ipc::CorbaSystemException( ERS_HERE, &ex ));	
    return TmFail ;
	}

  if ( l->length() == 0 ) {
    std::ostringstream txt ;
    txt<< "No classes loaded in the RDB server "<< (const char*)server_name;
    ers::error(ers::Message( ERS_HERE, txt.str()));	
    return TmFail ; 	
	}
  
  return TmPass ;
}
