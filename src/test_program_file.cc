#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>

#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

int main(int argc, const char * const * argv)
{
   CmdArgStr	filename	('f', "file", "file-name", "filename to test", CmdArg::isREQ );
   CmdArgBool	verbose         ('v', "verbose", "turn on 'verbose'-mode.");

   CmdLine  cmd(*argv, &filename, &verbose, NULL);
   cmd.description("Tests if a program file is readable and executable.") ;
   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);

   TestResult result = TmPass;

//separate the two file names and test them
   
    std::string fnames = (const char*)filename;
    std::string::size_type pos = fnames.find(";") ;
    std::string file1 = fnames.substr(0, pos);
    std::string file2 = fnames.substr(pos+1);
      
// check if the file exists, readable and executable	
   std::ostringstream txt ;
   if ( access(file1.c_str(), X_OK | R_OK) == -1 )
   {
	switch ( errno )
	{
		case EACCES:
 			txt<<"File "<<file1<<" does not have execute or read permissions." ;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		case ELOOP:
			txt<< "Too many symbolic links in resolving "<< file1;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		case ENOTDIR:
		case EFAULT:
		case ENOENT:
 			txt<< "File "<<file1<<" does not exist.";
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		default:
 			txt<< "Got '"<<strerror(errno)<<"' error trying to access file "<<file1;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmUnresolved ;
	}
   }
   else  //if binary file in RepositoryRoot area is found, do not continue 
   { 
       result = TmPass;   
       return result ;
   }    
       	
   if ( access(file2.c_str(), X_OK | R_OK) == -1 )
   {
	switch ( errno )
	{
		case EACCES:
 			txt<<"File "<<file2<<" does not have execute or read permissions." ;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		case ELOOP:
 			txt<<"Too many symbolic links in resolving "<< file2 ;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		case ENOTDIR:
		case EFAULT:
		case ENOENT:
 			txt<< "File "<<file2<<" does not exist.";
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmFail ;
			break ;
		default:
 			txt<< "Got '"<<strerror(errno)<<"' error trying to access file "<<file2;
 			ers::error (ers::Message( ERS_HERE, txt.str()));		
			result = TmUnresolved ;
	}
   }	
   else 
       result = TmPass;

   return result ;
}
