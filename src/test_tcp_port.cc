// Tests if a TCP/IP service is listening on a given port of a given host
// by establishing a TCP/IP connection to it.
// Unresolved if DNS lookup fails or address has invalid format.
// Detects ENOENT, EBADF, ETIMEDOUT and ECONNREFUSED TCP/IP errors.
//
// Author: A.Kazarov, 1999
//

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

static int atoport( const char* service, const char* proto = "tcp" )
{
   int			port;
   long int		lport;
   struct servent *	serv;
   char *		errpos;

  /* First try to read it from /etc/services */
  
   serv = getservbyname(service, proto);
   if (serv != NULL)
	port = serv->s_port;
   else 
   { 
   /* Not in services, maybe a number? */
	lport = strtol(service,&errpos,0);
	if ( (errpos[0] != 0) || (lport < 1) || (lport > 65535) )
		return -1; /* Invalid port address */
	port = htons(lport);
   }
   return port;
}

static struct in_addr * atoaddr(const char* address)
{
   struct hostent *		host;
   static struct in_addr	saddr;

  /* First try it as aaa.bbb.ccc.ddd. */
  
   saddr.s_addr = inet_addr(address);
   if ( saddr.s_addr != (in_addr_t)-1 )
	return &saddr;
   host = gethostbyname(address);
   if (host != 0)
	return (struct in_addr *) *host->h_addr_list;

   return 0;
}

int main(int argc, const char * const * argv)
{
   CmdArgStr 	hostname	('H',"Host", "host-name", "host name or IP address",CmdArg::isREQ);
   CmdArgStr 	service		('P',"Port", "port-number", "service name or port number", CmdArg::isREQ);
   CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");

   CmdLine  cmd(*argv, &hostname, &service, &verbose, NULL);
   cmd.description("Tests if a TCP/IP service is listening on a given port of a given host"
		   " by establishing a TCP/IP connection to it.\n"
		   "Unresolved if DNS lookup fails or address has invalid format.\n"
		   "Detects ENOENT, EBADF, ETIMEDOUT and ECONNREFUSED TCP/IP errors.") ;
   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);
   
   TestResult result = TmPass;

// here some testing goes
   
   int port = atoport( service ) ;
   if ( port == -1) 
   {
	std::ostringstream txt ;
 	txt<< "Can not resolve service number "<< (const char*)service;
	ers::warning(ers::Message( ERS_HERE, txt.str() ));	
	result = TmUnresolved ;
   }

   struct in_addr *	addr;
   struct sockaddr_in 	address;

   addr = atoaddr(hostname);
   if ( addr == 0 )
   {
	std::ostringstream txt ;
 	txt<< "Can not resolve host name "<< (const char*)hostname;
	ers::warning(ers::Message( ERS_HERE, txt.str() ));		
	result = TmFail ;
   }
   else
   {
	memset((char *) &address, 0, sizeof(address));
	
	address.sin_family 	= AF_INET;
	address.sin_port 	= port;
	address.sin_addr.s_addr = addr->s_addr;

	int sock = socket(AF_INET,SOCK_STREAM,0);
	int connected = connect(sock, (struct sockaddr *) &address, sizeof(address));
        std::ostringstream txt ;
	if ( connected == -1 )
		switch ( errno )
		{	
			case ENOENT:
			case EBADF:
				break ;
			case ECONNREFUSED:
			case ETIMEDOUT:
			case ENETUNREACH:
			case EHOSTUNREACH:
			case EHOSTDOWN:
 				txt<<"TCP Test failed: " << strerror(errno) ;
				ers::warning(ers::Message( ERS_HERE, txt.str() ));		
				result = TmFail ;	
				break ;
			default:
 				txt<<"TCP Test unresolved: " << strerror(errno) ;
				ers::warning(ers::Message( ERS_HERE, txt.str() ));				
				result = TmUnresolved ;
				break ;
        	}
	else 
		close(sock) ;
   }

   return result;
}
