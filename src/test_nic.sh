#!/bin/sh

passed_value=0
failed_value=183
unresolved_value=184

address="127.0.0.1"
verbose="no"
packetsize="1024"
counter="1"
timeout="10"

trap "echo \"Terminated by a signal. The test failed.\"; exit ${failed_value}" 	1 2 6 11 

function parse_cmdl
{
while (test $# -gt 0 ); do
  case "$1" in
    -v)
	verbose="yes"
	shift
    ;;

    -a)
	shift
	address=${1}
 	shift
    ;;

    -c)
	shift
	counter=${1}
 	shift
    ;;

    -t)
	shift
	timeout=${1}
 	shift
    ;;

    -h|--help)
	usage ; exit $unresolved_value ;
    ;;

    *)
        echo "Wrong parameters"; usage ; exit $unresolved_value ;
    ;;
 esac
done
}

function usage {
echo "Usage: test_nic.sh [-v] -a <ip_address> [-c counter=1] [-t timeout=10]"; 
echo "Pings the closest router using the NIC interface for specified IP address." ;
echo "Sends <counter> number of packets 1024 bytes each, default number is 1." ;
echo "NB: each next packet is sent with 0.3 second delay." ;
echo "Exits with failed value if not all echo replies recieved within [10 sec] <timeout>." ;
}

[ $# -lt 1 ] && { usage; exit $unresolved_value ; }

export PATH="/bin:/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/usr/local/sbin"

parse_cmdl $*
command1='sed s#\\.[0-9]*\$##g '
command="echo $address | $command1"
network=`eval $command`.1

# hack for preseries, ping my own local interface
case `uname -n` in
	*preseries*)
	network="127.0.0.1"
	;;
esac

sleep 0.$[ ( $RANDOM % 5 )  + 1 ]

res=`ping -c $counter -i 0.3 -w $timeout -I $address -t 1 $network -s $packetsize 2>&1`
[ $? = "0" ] || { echo "Failed to send $counter packets to network $network (interval 0.5s) in $timeout seconds." ; echo "Reason: $res" ; exit ${failed_value} ; }

[ $verbose = "yes" ] && { echo "Test passed"; echo $res ; }

exit ${passed_value} 

