/*	Tests functionality of the named RDB server in the given partition.
	Retrieves number of classes from the server. Test failed if this number is 0.
	Should be run on ahy host in IPC environment.

	A.Kazarov,	2000
	Dec 2003: new ORB/IPC
*/

#include <iostream>
#include <string>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <tmgr/tmresult.h>
#include <rdb/rdb.hh>

using namespace daq::tmgr ;

int main(int argc, char ** argv)
{
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

  CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work in");
  CmdArgStr	server_name	('n', "name", "server-name", "RDB server name");
  CmdArgBool	verbose 	('v', "verbose", "turn on 'verbose'-mode.");
  CmdArgStr     db_var_name	('a', "db-variable-name", "variable-name", "read database server name form process environment with given name instead of -n option");
  verbose=false; 
  CmdLine  cmd(*argv, &verbose, &partition_name, &server_name, &db_var_name, NULL) ;
  cmd.description("Tests functionality of named RDB server in the given partition.\n"
		  "Checks if server is available and has non-zero number of classes loaded."
		  "Can be run on ahy host in IPC environment.") ;
  CmdArgvIter  arg_iter(--argc, ++argv) ;
  
  server_name = "" ;

  cmd.parse(arg_iter) ;

  if( !(server_name.flags() & CmdArg::GIVEN) ) {
	if( !(db_var_name.flags() & CmdArg::GIVEN) ) {
		ers::error (ers::Message( ERS_HERE,"RDB server name is not provided (use -n or -a option)" ));		
		return TmUnresolved ;
	} else	{
		const char * v = getenv( static_cast<const char *>(db_var_name) );
		if ( v && *v ) {
			{ server_name = v; }
		if ( verbose )
			{ ERS_LOG("Server name as read from environment: " << server_name ); }
		}
		else {
		std::ostringstream txt ;
		txt<<"Server name is not found since the environment variable " << static_cast<const char *>(db_var_name) << " mentioned by command line is not set or is empty.";
		ers::error (ers::Message( ERS_HERE, txt.str()));		        		
			return TmUnresolved ;
		}
	}
  }

  IPCPartition p(partition_name) ;
  if ( !p.isValid() ) {
   		std::ostringstream txt ;
 		txt << "Cannot find IPC partition "<< (const char*)partition_name ;
 		ers::error (ers::Message( ERS_HERE, txt.str()));		
	        return TmFail ;  		
	}
	
	// find server object in partition
  rdb::cursor_var cursor ;
  try 	{
	cursor = p.lookup<rdb::cursor> (std::string(server_name)) ;
	}
  catch ( daq::ipc::Exception & ex ) {
   	std::ostringstream txt ;
 	txt << "RDB server " << server_name << " not registered in partition " << (const char*)partition_name << ": " << ex.what() ;
	ers::error( ers::Message( ERS_HERE, txt.str()) ) ;
	return TmFail ;
	}
         
/*  cursor -> reload_database(&s) ;
  if ( s.returnCode != rdbReply_Success )
	{
	std::cerr << "ERROR: Can not reload database." << std::endl;
        std::cerr << "REASON: " << s.returnCode ;
	if ( s.returnCode == rdb_E_CannotProceed )
		{
		std::cerr << " : Can not load file \"" << s.values.rdb_E_CannotProceed_Value -> file << "\"";
		}
	std::cerr << std::endl ;
	return TmFail ;
	}
*/

  rdb::RDBClassList_var l ;
  try {
	cursor->get_all_classes(l);
	}
  catch ( CORBA::SystemException & ex ) {
	ers::warning(daq::ipc::CorbaSystemException( ERS_HERE, &ex ));	
	return TmFail ;
	}

  if ( l->length() == 0 ) {
	std::ostringstream txt ;
 	txt<< "No classes loaded in the RDB server "<< (const char*)server_name;
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	return TmFail ; 	
	}
  
  return TmPass ;
}
