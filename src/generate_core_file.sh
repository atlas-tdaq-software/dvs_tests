#!/bin/bash
# ------------------------------------------------------------------
# [kolos] Core file generation script
#         Produces core file for a running process
# ------------------------------------------------------------------

export PATH=/bin:/usr/bin:/sbin:$PATH

VERSION=0.0.1
SUBJECT=some-unique-id
USAGE="Usage: command [-v] [-h] [-d <directory_name>] -p <partition_name> -a <app_name> [-m mail_address]"

# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

APP_NAME=""
PARTITION_NAME=""
DIRECTORY_NAME="/logs/cores"
EMAIL_ADDRESS="Andrei.Kazarov@cern.ch"

while getopts ":a:d:p:vhm:" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0
        ;;
      "a")
        APP_NAME=$OPTARG
        ;;
      "d")
        DIRECTORY_NAME=$OPTARG
        ;;
      "p")
        PARTITION_NAME=$OPTARG
        ;;
      "m")
        EMAIL_ADDRESS=$OPTARG
        ;;
      "h")
        echo $USAGE
        exit 0
        ;;
      "?")
        echo "error: Unknown option $OPTARG"
        exit 1
        ;;
      ":")
        echo "error: No argument value for option $OPTARG"
        exit 1
        ;;
      *)
        echo "error: Unknown error while processing options"
        exit 1
        ;;
    esac
  done

shift $(($OPTIND - 1))

# --- Mandatory Arguments -----------------------------------------
if [ -z "$APP_NAME" ]; then
   echo "error: -a <app_name> is missing"
   echo $USAGE
   exit 1
fi

if [ -z "$PARTITION_NAME" ]; then
   echo "error: -p <partition_name> is missing"
   echo $USAGE
   exit 1
fi

# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "error: Script is already running"
   exit 1
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# --- Body --------------------------------------------------------

INFO_NAME=`hostname`.$APP_NAME
PID=`is_ls -p $PARTITION_NAME -n PMG -r $INFO_NAME -vN|grep pid|awk '{print $2}'`
BINARY_NAME=`ps -p $PID -o comm=`
CORE_FILE_NAME=$DIRECTORY_NAME/$PARTITION_NAME.$APP_NAME.$BINARY_NAME.$PID

if [ -f "$CORE_FILE_NAME" ]; then
   echo "warning: core file for this process already exists: $CORE_FILE_NAME"
   exit 0
fi

gcore -o $DIRECTORY_NAME/$PARTITION_NAME.$APP_NAME.$BINARY_NAME $PID
host=`/bin/uname -n`
echo "Core file $DIRECTORY_NAME/$PARTITION_NAME.$APP_NAME.$BINARY_NAME produced on $host" | /bin/mail -s "core file produced" $EMAIL_ADDRESS -r 
   
# -----------------------------------------------------------------
