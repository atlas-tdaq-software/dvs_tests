/* This test tests availability of system resources on the computer by */
/* allocating large amount of memory and creating large file in temp area */
/* A.Kazarov, 2000 */

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <iostream>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

const int DEF_MEM_SIZE  = 256 ; // Mb
const int DEF_FILE_SIZE = 128 ; // Mb

int main(int argc, const char * const * argv)
{
   CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");
   CmdArgInt	memsize		('m', "memsize", "memory-size", "memory size to allocate (Mb)");
   CmdArgInt	filesize	('f', "filesize", "file-size", "size of file to allocate (Mb)");
   verbose=false;

   CmdLine  cmd(*argv, &verbose, &filesize, &memsize, NULL);
   cmd.description("Tests functionality of a host by allocating and filling some block of memory"
		   "and creating file in /tmp area.") ;
   CmdArgvIter  arg_iter(--argc, ++argv);
   cmd.parse(arg_iter);

   int memsz = ( memsize.flags() && CmdArg::GIVEN ? memsize : DEF_MEM_SIZE ) << 20 ;
   int filesz = ( filesize.flags() && CmdArg::GIVEN ? filesize : DEF_FILE_SIZE ) << 20 ;
   
   TestResult result = TmPass;

   if ( verbose ) 
		ERS_LOG( "Allocating " << memsz <<" bytes of memory..." );

   char* p = new char[memsz] ;
   if ( !p ) 
   	{
	  std::ostringstream txt ;
 	  txt<<"Can't allocate " << memsz << " bytes of memory: " << strerror(errno) ;
	  ers::error(ers::Message( ERS_HERE, txt.str()));	
 	}

   if ( !memset (p, 0xFF, memsz) )
	{
	  std::ostringstream txt ;
 	  txt<<"Can't allocate " << memsz << " bytes of memory: " << strerror(errno) ;
	  ers::error(ers::Message( ERS_HERE, txt.str()));	
	  delete p ;
	  return TmFail ;
	};

   if ( verbose ) 
		ERS_LOG("OK");
	 
   char fname[256] ;
   sprintf(fname, "/tmp/test-host-%d", getpid() ) ;

   if ( verbose ) 
		ERS_LOG( "Creating file of size " << filesz << " bytes..." );

   int fd = creat(fname, 0) ;
   
   if ( fd == -1 ) 
   	{
	  std::ostringstream txt ;
 	  txt<< "Can't create file "<<fname<<": "<< strerror(errno);
	  ers::error(ers::Message( ERS_HERE, txt.str()));	
	  delete p ;
	  return TmFail ;
	}

   if ( ftruncate(fd, filesz) == -1 )
   	{
	  std::ostringstream txt ;
 	  txt<<"Can't change file size: "<< strerror(errno) ;
	  ers::error(ers::Message( ERS_HERE, txt.str()));	

	  delete p ;
	  close(fd) ;
	  unlink(fname) ;
	  return TmFail ;
	}

   delete p ;
   ftruncate(fd,0);
   close(fd) ;
   unlink(fname) ;
   return result ;
}
