#ifndef DVSTESTS_EXCEPTIONS_H
#define DVSTESTS_EXCEPTIONS_H



#include <string>
#include <ers/Issue.h>
#include <ers/ers.h>

namespace daq {

    /*! \class tmgr::TestFailed
     *	Exeption to be used as base exception for all exceptions produced by concrete tests for tdaq components.
     */

  ERS_DECLARE_ISSUE(
    tmgr,
    TestFailed,
    "Test " << test_id << " FAILED: " << reason,
    ((std::string)test_id)
    ((std::string)reason)
  )

} // namespace daq

// #define TMGR_TEST_FAILED(test,message)    { daq::tmgr::TestFailed tf(ERS_HERE,test,message) ; ers::error(tf) ; return TmFail ; }

#define TMGR_TEST_FAILED(test,message,ex) { daq::tmgr::TestFailed tf(ERS_HERE,test,message,ex) ; ers::error(tf) ; return TmFail ; }

#endif
