#!/bin/sh

failed_value=183
unresolved_value=184

filesize=0
uselimit=98
maxload=10
verbose="no"
logdir="/tmp"
dbdir=${TDAQ_DB_PATH}
swdir="${TDAQ_INST_PATH}"

trap "echo \"Terminated by a signal. The test failed.\"; exit ${failed_value}" 	1 2 6 11 

function parse_cmdl
{
while (test $# -gt 0 ); do
  case "$1" in
    -ul)
	shift
	uselimit=${1}
	shift
    ;;

    -v)
	verbose="yes"
	shift
    ;;

    -fs)
	shift
	filesize=${1}
 	shift
    ;;

    -ml)
	shift
	maxload=${1}
 	shift
    ;;
    
     -ld)
	shift
	logdir=${1}
 	shift
    ;;
    
     -sd)
	shift
	swdir=${1}
 	shift
    ;;
    
     -dd)
	shift
	dbdir=${1}
 	shift
    ;;
     -h|--help)
	usage; exit $unresolved_value ; 
    ;;
 esac
done
}

function usage {
echo "test_host_system.sh: Tests that the computer is ready to run TDAQ applicaitons. Checks the filesystems mounts, usage and host load."
echo "Usage: test_host.sh [-v] [-fs filesize] <-ld logdir> <-ul uselimit> <-ml maxload>" 
echo "Parameters: "
echo "[-fs filesize] : if passed, then the file is created in /tmp filesystem. If not, 0 file size is created."
echo "<-ld logdir> : directory for partition logs (\$TDAQ_LOGS_PATH)"
echo "<-ul uselimit> : maximum allowed filesystem usage in percents, e.g. 98"
echo "<-ml maxload> : maximum allowed system load, as reported by to, e.g. 10" ;
}

[ $# -lt 1 ] && { usage; exit $unresolved_value ; }

export PATH="/bin:/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/usr/local/sbin"

parse_cmdl $*

#[ -z $TDAQ_LOGS_PATH ] && \
#{ echo "TDAQ_LOGS_PATH not defined"; exit $unresolved_value ; }

function echo
{
[ $verbose = "yes" ] && /bin/echo $1
}

echo "creating a file of size ${filesize}Mb in $logdir ..."

filename=$$.testfile
[ -d $logdir ] || 
	{ echo "Log directory for the partition does not exist: $logdir "; exit $failed_value ; }
touch $logdir/$filename || 
	{ echo "Failed to create $logdir/$filename "; exit $failed_value ; }
[ ${filesize} -gt 0 ] && { dd if=/dev/zero of=$logdir/$filename bs=${filesize}M count=1 2>/dev/null || \
	{ echo "Failed!"; exit $failed_value ; } ; }
rm $logdir/$filename || \
	{ echo "Failed to remove temporary file $TDAQ_LOGS_PATH/$filename"; exit $failed_value ; }
echo "OK"
echo
echo "creating a file of size ${filesize}Mb in /tmp ..."

touch /tmp/$filename || 
	{ echo "Failed to create /tmp/$filename "; exit $failed_value ; }
[ ${filesize} -gt 0 ] && { dd if=/dev/zero of=/tmp/$filename bs=${filesize}M count=1  2>/dev/null  || \
	{ echo "Failed!"; exit $failed_value ; } ; }

rm /tmp/$filename || \
	{ echo "Failed to remove temporary file /tmp/$filename"; exit $failed_value ; }
echo "OK"
echo
echo "testing filesystems capacity (OK if less then ${uselimit}%): "
command="df -P | grep -v Capacity | awk '{ if (\$5 > \"${uselimit}%\" )  print \$1 \" is FULL:  \" \$5  ; if (\$5 > \"${uselimit}%\" ) exit 1 ; }'"
#echo $command
eval ${command} || { echo "Failed!"; exit $failed_value ; }

echo "OK"
echo
echo "testing system overall load (OK if less then ${maxload}): "
command="uptime | tr ',' ' ' | awk '{ if (\$(NF-2) > \"${maxload}%\" )  print \"Average load is too HIGH:  \" \$(NF-2)  ; if (\$(NF-2) > \"${maxload}%\" ) exit 1 ; }'"
eval ${command} || { echo "Failed!"; exit $failed_value ; }
echo "OK"
echo

echo "testing availablility of s/w installation in ${swdir}"
cd ${swdir} || { echo "Failed to access s/w installation in ${swdir}"; exit $failed_value ; }
test -d ${swdir}/share/bin || { echo "TDAQ s/w installation not found in ${swdir}"; exit $failed_value ; }
echo "OK"
echo

#echo "Databases: $dbdir" 
#echo "testing availablility of database areas"
#for d in `echo $dbdir | tr ':' ' '`; do echo -n "$d :"; cd $d && test -d $d || { echo "Failed! "; exit $failed_value ; } ; echo "OK"; done
#echo "OK"

echo "Test passed"
exit 0 

