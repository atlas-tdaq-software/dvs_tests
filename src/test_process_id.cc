/*	Tests existence of a process on Unix by sending a signal (kill -0) to it via remote login command

	A.Kazarov, 2000
*/

#include <cstdlib>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <cstdio>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

int main(int argc, const char * const * argv)
{
   CmdArgInt	pid		('P', "Pid",		"process-id", "process ID to test", CmdArg::isREQ );
   CmdArgStr 	hostname	('H', "Host",		"host-name", "host name or IP address",CmdArg::isREQ);
   CmdArgStr 	login_cmd	('l', "logincmd",	"login-command", "Remote shell command (rsh or ssh)",CmdArg::isREQ);
   CmdArgBool	verbose		('v', "verbose",	"turn on 'verbose'-mode.");

   CmdLine  cmd(*argv, &pid, &verbose, &hostname, &login_cmd, NULL);
   cmd.description("Tests existence of a process on Unix by sending a signal (kill -0) to it via remote login command.") ;
   CmdArgvIter  arg_iter(--argc, ++argv);

   cmd.parse(arg_iter);
     
   char command[64] ;

   sprintf(command, "%s %s \"kill -0 %d\" 2>&1", (const char *)login_cmd, (const char *)hostname, (int)pid ) ;

   FILE* stream = popen(command,"r") ;
   if ( !stream ) // test was not executed
   	{
   	std::ostringstream txt ;
 	txt<< "Can not execute command "<< command;
 	ers::error (ers::Message( ERS_HERE, txt.str()));		
   	return TmUnresolved ;
	}

   int result = TmPass ;
   char buf[128] ;

   if ( !fread(buf,1,127,stream) )  // empty buffer => process exist
   	{
	if ( verbose )
		ERS_LOG("OK: Process with PID "<<(int)pid<<" was found on the host "<<(const char*)hostname<<" and it accepts signals." ) ;
   	}
   else				// s.t. like "kill: (31673) - No such pid" in the output
   	{
	if ( strstr(buf,"No such pid") ) // => no process
		{
   		std::ostringstream txt ;
 		txt<< "Process with PID "<<(int)pid<<" was NOT found on the host "<< (const char*)hostname;
 		ers::error (ers::Message( ERS_HERE, txt.str()));		
		result = TmFail ;
		}
	else				// some additional output present, but process is OK
		{
		if ( verbose )
			ERS_LOG("OK: Process with PID "<<(int)pid<<" was found on the host "<<(const char*)hostname<<" and it accepts signals." );
		}
   	}
	
   pclose(stream) ;	
   return result ;
}
