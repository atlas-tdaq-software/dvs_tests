/*
Tests MRS server in a given partition by subscribing for test messages and sending them.

    S.Kolos, 1999

    A.Kazarov, Dec 2003: new IPC/ORB

*/

#include <signal.h>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/alarm.h>
#include <mrs/message.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

static CmdArgBool	verbose ('v', "verbose", "turn on 'verbose'-mode.");
static short		errCount;
static short		callbackCount;
static const double	timeout_period = 5.0;
static MRSReceiver *	Receiver;
static std::string	expression = "DVS_TEST_" ;

static void callback(MRSCallbackMsg * mc){
	callbackCount++;
	if ( verbose )
		ERS_INFO( "CALLBACK:: "<< mc->getMessage() );
	
	if( callbackCount == 3 )
		kill(getpid(), SIGINT);
}

static bool timeout_callback(void * ){
	ers::warning(ers::Message( ERS_HERE, "Timeout occured when waiting for a mrs message" ));
	kill(getpid(), SIGINT);

        return false ;
}

static bool Test(void * param)
{
  IPCPartition * p = (IPCPartition*)param;

  MRSStream mout(*p);
    
  mout << "TEST_MESSAGE" << MRS_DIAGNOSTIC << MRS_QUALIF(expression.c_str()) 
  			 << MRS_TEXT("This is test message, please ignore it") 
  			 << ENDM;
  mout << "TEST_MESSAGE" << MRS_DIAGNOSTIC << MRS_QUALIF(expression.c_str()) 
  			 << MRS_TEXT("This is test message, please ignore it") 
  			 << MRS_OPT_PARAM<int>("param1", "Passing integer %d", 44) 
  			 << ENDM;
  mout << "TEST_MESSAGE" << MRS_DIAGNOSTIC << MRS_QUALIF(expression.c_str())
  			 << MRS_TEXT("This is test message, please ignore it") 
  			 << MRS_OPT_PARAM<float>("param1", "Passing float PI = %f", 3.14) 
  			 << ENDM;
  return false ;
}

static void DoTest(IPCPartition & p)
{
  Receiver = new MRSReceiver(p);
  
  try { Receiver->subscribe(expression.c_str(),callback) ; }
  catch ( daq::mrs::Exception & ex )
  {
	std::ostringstream txt ;
 	txt<<"Subscribe to "<<expression<<" fails: ";
	ers::warning(ers::Message( ERS_HERE, txt.str(), ex));	
	errCount++;
  }
  
  if ( verbose )
 		ERS_INFO("Subscribe to "<<expression<<" successfull" );
  
  IPCAlarm * to_alarm = new IPCAlarm(timeout_period, timeout_callback, 0) ;
  IPCAlarm * do_alarm = new IPCAlarm(0, Test, &p) ;

  signal(SIGINT, [](int) {; }) ;
  ::sleep(-1) ;

  delete to_alarm ;
  delete do_alarm ;
  
  try { Receiver->unsubscribe(expression.c_str()) ; }
  catch ( daq::mrs::Exception & ex )
  {
	std::ostringstream txt ;
 	txt<<"Unsubscribe to "<<expression<<" fails: ";
	ers::warning(ers::Message( ERS_HERE, txt.str(), ex));	
	errCount++;
  }

  if ( verbose )
	ERS_INFO("Unsubscribe to "<<expression<<" successfull" );
		  
  if ( callbackCount != 3 )
  {
		std::ostringstream txt ;
 	        txt<<"Wrong number of callback invocations : "<< callbackCount;
	 	ers::warning(ers::Message( ERS_HERE, txt.str() ));	
  }
  else
  {
  	if ( verbose )
		ERS_INFO("Callback invoked "<<callbackCount<<" times." );
  }
}

int main(int argc, char ** argv)
{
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

      // Declare arguments
      
  CmdArgStr	partition_name('p', "partition", "partition-name", "Partition to work in.");
 
       // Declare command object and its argument-iterator
  CmdLine  cmd(*argv, &verbose, &partition_name, NULL);
  cmd.description("Tests MRS server for the partition by sending and receiving test messages.") ;
  CmdArgvIter  arg_iter(--argc, ++argv);
  
       // Parse arguments
  cmd.parse(arg_iter);

  IPCPartition		p(partition_name);

  if ( !p.isValid() )
	{
	std::ostringstream txt ;
 	txt<<"Cannot access partition server for partition "<< (const char*)partition_name ;
	ers::warning(ers::Message( ERS_HERE, txt.str()));	
	return TmFail;  		
	}
 
    
  char pid[32];
  char hostname[256];
  hostname[0] = 0;
  
  gethostname(hostname, sizeof(hostname));
  sprintf(pid,"%lu",(unsigned long)getpid()+1);
  
  expression += pid; 
  
  DoTest(p);

  TestResult ret = TmPass;

  if ( callbackCount != 3 )
  	ret = TmUnresolved;

  if ( errCount || callbackCount == 0 )
  	ret = TmFail;
	
  return ret;
}
