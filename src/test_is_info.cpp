/*
Tests is a named IS info is published on IS server (seeded to test functionality of cdi-set_conditions)

     A.Kazarov, Feb 2005

*/

#include <iostream>
#include <cstdio>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/alarm.h>
#include <is/infoany.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

int main(int argc, char ** argv)
{
try	{ IPCCore::init( argc, argv ) ; }
catch ( daq::ipc::Exception & ex )
	{
	ers::error( ex ) ;
	exit(1) ;
	}

CmdArgBool	verbose ('v', "verbose", "turn on 'verbose'-mode.");
//CmdArgStr	server_name('n', "server", "server-name", "server to test.", CmdArg::isREQ);
CmdArgStr	info_name('i', "info", "info-name", "IS info name to test.", CmdArg::isREQ);
CmdArgStr	partition_name('p', "partition", "partition-name", "Partition to work in.");

CmdLine 	cmd(*argv, &verbose, &partition_name, &info_name, NULL) ;
cmd.description("Tests if a named IS info is published in IS system.") ;
CmdArgvIter  	arg_iter(--argc, ++argv) ;
cmd.parse(arg_iter);

IPCPartition		p(partition_name);
if ( !p.isValid() )
	{
	ers::error( ers::Message( ERS_HERE,"Cannot access partition IPC server")) ;
	return TmFail;  		
	}

ISInfoDictionary dict(p) ;

try 	{
	if ( dict.contains(std::string(info_name)) )
		return TmPass ;
	else {
   		std::ostringstream txt ;
 		txt<<"Info " << info_name << " is not found in Information Service";
 		ers::error (ers::Message( ERS_HERE, txt.str()));		
	}
	}
catch ( daq::is::Exception & ex )
	{
   		std::ostringstream txt ;
 		txt<< "Info " << info_name << " is not found in Information Service: " << ex;
 		ers::error (ers::Message( ERS_HERE, txt.str()));		
	}
	
return TmFail;  		
}
