#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include <is/infoT.h>


int main(int argc, const char * const * argv)
{	

  CmdArgInt     base_address	('B', "BaseAddress", "base-address", "physical address of the module");
  CmdArgInt 	comm_address	('C', "CommAddress", "communication-address", "communication address od the module");
  CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");
//  CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work in.");
  CmdArgStr	module_name	('m', "module", "module-name", "module name to test.",CmdArg::isREQ);
  CmdArgStr	crate_name	('c', "crate", "crate-name", "crate name to test.",CmdArg::isREQ);
//  CmdArgStr	param_name	('P', "param", "parameter-name", "parameter name to test.",CmdArg::isREQ);
//  CmdArgStr	is_name		('i', "ISserver", "crate-name", "crate name to test.",CmdArg::isREQ);

  CmdLine  cmd(*argv, &crate_name, &module_name, &base_address, &comm_address, &verbose, NULL);
  cmd.description("Tests VME interface for the module");

  base_address = 0;
  comm_address = 0;
  
       // Parse arguments
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.parse(arg_iter);
  
// not implemented now, always returns "PASSED"  
  
/*  
  IPCPartition		p(partition_name);
    
  ISInfoDictionary	id(p);
  
  // construct information name
  
  RWCString name(is_name);
  name += ".";
  name += crate_name;
  name += ".";
  name += module_name;
  name += ".";
  name += param_name;
 


  // get the value of information objects on is_name IS server
  
  ISInfo::Status	status;
  ISInfoInt		value1;
  
  if ( ( status = id.findValue( name, value1) ) != ISInfo::Success )
  {
  	if ( verbose ){
		std::cerr << "ERROR:: findValue returns " << status << std::endl;
  	}
	return TmFail;	
  }
  
  // The information for the module's parameter shall be updated each 3 seconds
  
  sleep(5);
  
  // again get the value of information objects on is_name IS server

  ISInfoInt		value2;
  if ( ( status = id.findValue( name, value2) ) != ISInfo::Success )
  {
  	if ( verbose ){
		std::cerr << "ERROR:: findValue returns " << status << std::endl;
  	}
	return TmFail;	
  }
  
  if ( value1 == value2 )
  {
  	if ( verbose ){
		std::cerr << "ERROR:: value was not changed during 5 seconds " << std::endl;
  	}
	return TmFail;	
  }
*/  
  return TmPass;
}
