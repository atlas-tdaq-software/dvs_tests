/*
Tests IPC server for the given partition by publishing and accessing an object there

    S.Kolos, 1999

    A.Kazarov, Dec 2003: new IPC/ORB
*/
#include <unistd.h>
#include <cstdio>

#include <string>
#include <list>
#include <iostream>
#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/object.h>
#include <ipc/partition.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace daq::tmgr ;

static std::string TestObjectName = "TestObject_";

class TestObject: public IPCNamedObject<POA_ipc::servant, ipc::single_thread>
{
public:
	TestObject(IPCPartition & p) :
		IPCNamedObject<POA_ipc::servant, ipc::single_thread>(p, TestObjectName) { ; } ;

void shutdown() { ; } ;
};

int main(int argc, char ** argv)
{	
 
  CmdArgStr     partition_name	('p', "partition", "partition-name", "partition to test.");
  CmdArgBool	verbose		('v', "verbose", "turn on 'verbose'-mode.");
  verbose=false;
  try
	{ IPCCore::init( argc, argv ) ; }
  catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return TmUnresolved ;
	}
  catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

       // Declare command object and its argument-iterator
  CmdLine  cmd(*argv, &partition_name, &verbose, NULL);
  cmd.description("Tests IPC server for the given partition by publishing an object there.\n"
		  "Can be run on ahy host in IPC environment.") ;
  CmdArgvIter  arg_iter(--argc, ++argv);

       // Parse arguments
  cmd.parse(arg_iter);
    
  TestResult result = TmPass;

  char pid[32];
  char hostname[256];
  hostname[0] = 0;
  
  gethostname(hostname, sizeof(hostname));
  sprintf(pid,"%lu",(unsigned long)getpid());
  
  TestObjectName += hostname;
  TestObjectName += "-";
  TestObjectName += pid; 
  
  IPCPartition p = (const char*)partition_name;

  if ( !p.isValid() )
	{
	std::ostringstream txt ;
 	txt<<"Cannot access partition server for partition "<< (const char*)partition_name ;
	ers::error(ers::Message( ERS_HERE, txt.str()));	
	return TmFail;  		
	}

  TestObject* test_object = new TestObject(p);

  if ( verbose )
	ERS_LOG("Object created, publishing it ..." );
  
  try 	{
	test_object->publish() ;

 	if ( verbose )
		{ ERS_LOG("Object published, accessing it ..." ); }

	ipc::servant_var object = p.lookup<ipc::servant>( TestObjectName );

 	if ( verbose )
		ERS_LOG("Object found, accessing it ..." );

	ipc::servant::ApplicationContext_var info = object->app_context() ;
	}
	catch ( const daq::ipc::Exception & ex ) {
		ers::warning( ex ) ;
		result = TmFail ;
	}
  	catch ( CORBA::SystemException & ex ) {
		ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &ex));	
     	result = TmFail ;
	}

  	if ( result != TmFail ) {
		if ( verbose )
			{ ERS_LOG("Removing object publication ...") ; }

		try { test_object->withdraw() ; }
		catch ( daq::ipc::Exception & ex )	{
			ers::warning( ex );
			result = TmFail;
			}  	
	}

  test_object->_destroy() ;
  return result ;
}
